const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env) => {
  return {
    entry: './src/App.tsx',
    target: 'web',
    mode: env.NODE_ENV,
    output: {
      path: path.resolve(__dirname, 'build'),
      filename: 'bundle.js',
    },

    devServer: {
      port: 8081,
      historyApiFallback: true
    },
    resolve: {
      extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
    },
    module: {
      rules: [
        // {
        //   test: /\.css$/i,
        //   use: ['style-loader', 'css-loader'],
        // },
        {
          test: /\.(png|woff|woff2|eot|ttf|svg)$/,
          use: 'url-loader?limit=100000',
        },
        {
          test: /\.s[ac]ss$/i,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
          test: /\.(ts|tsx)$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'src', './public/index.html'),
      }),
      new webpack.DefinePlugin({
        'process.env': {
          ENV: JSON.stringify(env.NODE_ENV),
          S3: JSON.stringify(env.S3),
        },
      }),
    ],
  }
};