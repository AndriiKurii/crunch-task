import * as React from 'react';
import axios from 'axios';
import { useParams } from 'react-router';
import Next from '../../public/next.png';
import Previous from '../../public/previous.png';
import './Templates.scss';

export const Templates = () => {
  const { id } = useParams();
  const [templates, setTemplates] = React.useState([]);
  const [current, setCurrent] = React.useState(null);
  const [cursor, setCursor] = React.useState(0);

  React.useEffect(() => {
    fetch();
  }, [])

  const fetch = async () => {
    const { data: { templates } } = await axios.get('http://localhost:8000/api/templates');

    const initCurrent = templates.find(i => i.id == id)?.id;
    if (initCurrent) {
      setCurrent(initCurrent);
    } else {
      setCurrent(templates[0]?.id)
    }

    setTemplates(templates);
  }

  const selected = templates.find(i => i.id == current);

  return <div className="templates">
    <div className="large">
      {selected ? <React.Fragment>
        <img src={selected.imageURL}
          alt="Large Image"
          width="430"
          height="360" />
        <div className="details">
          <p><strong>Title</strong> {selected.title}</p>
          <p><strong>Description</strong> {selected.description}</p>
          <p><strong>Cost</strong> ${selected.cost}</p>
          <p><strong>ID #</strong> {selected.id}</p>
          <p><strong>Thumbnail File</strong>{selected.thumbnail}</p>
          <p><strong>Large Image File</strong> {selected.image}</p>
        </div>
      </React.Fragment> : null}
    </div>
    <div className="thumbnails">
      <img
        className={`previous ${cursor === 0 ? 'disabled' : ""}`}
        src={Previous}
        alt="previous"
        onClick={() => setCursor(Math.max(cursor - 1, 0))}
      />

      <div className="group">
        {templates.slice(cursor, cursor + 4).map(i => (
          <a
            onClick={() => setCurrent(i.id)}
            className={`${current == i.id ? 'active' : ""}`}
            key={i.id}
            title={i.title}
          >
            <img
              src={i.thumbnailURL}
              width="145"
              height="121"
            />
            <span>{i.id}</span>
          </a>
        ))}
      </div>

      <img
        className={`next ${cursor === templates.length - 3 ? 'disabled' : ""}`}
        src={Next}
        alt="next"
        onClick={() => {
          setCursor(Math.min(cursor + 1, templates.length - 3))
        }} />
    </div>
  </div>
}