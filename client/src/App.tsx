import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import { Templates } from './pages';
import { Header, Footer } from './atoms'
import './styles.scss'

const App = () => {
  return <div id="container">
    <Header />
    <Routes>
      <Route path='/:id' element={<Templates />}></Route>
      <Route path='*' element={<Templates />}></Route>
    </Routes>
    <Footer />
  </div>
}

const container = document.getElementById('root');
const root = createRoot(container);
root.render(<BrowserRouter><App /></BrowserRouter>);