const { Joi } = require('express-validation')

const addTemplateValidation = {
  body: Joi.object({
    title: Joi.string().required(),
    cost: Joi.number().required(),
    description: Joi.string().required(),
    thumbnail: Joi.string().required(),
    image: Joi.string().required()
  }),
}

module.exports = {
  addTemplateValidation
}