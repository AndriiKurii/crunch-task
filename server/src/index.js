const express = require('express');
const cors = require('cors')

const initConnection = require('../db-connection');
const templates = require('./routes/templates');

const app = express();

app.use(express.json());
app.use(cors());
app.use('/api/templates', templates)

initConnection();

app.listen(8000, () => {
  console.log("server started")
})