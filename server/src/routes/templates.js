const express = require('express');
const { validate } = require('express-validation')
const Template = require('../models/Template');
const { addTemplateValidation } = require('../validation/templates');
const { bucket } = require('../../config');

const router = express.Router();

router.get('/', async (req, res) => {
  const templates = await Template.find({});
  const templatesWithURL = templates.map((item) => {
    return {
      id: item.id,
      title: item.title,
      cost: item.cost,
      description: item.description,
      image: item.image,
      thumbnail: item.thumbnail,
      imageURL: `${bucket}large/${item.image}`,
      thumbnailURL: `${bucket}thumbnails/${item.thumbnail}`
    }
  })
  return res.status(200).json({
    templates: templatesWithURL
  });
})

router.post('/', validate(addTemplateValidation), async (req, res) => {
  const template = new Template({
    id: item.id,
    title: req.body.title,
    cost: req.body.cost,
    description: req.body.description,
    thumbnail: req.body.thumbnail,
    image: req.body.image
  })

  template.save();
  return res.status(200).json({
    templates: true
  });
})

module.exports = router;