const mongoose = require('mongoose');
const { Schema } = mongoose;

const templateSchema = new Schema({
  title: {
    type: String
  },
  cost: {
    type: Number
  },
  id: {
    type: Number
  },
  description: {
    type: String
  },
  thumbnail: {
    type: String
  },
  image: {
    type: String
  },
})

module.exports = mongoose.model('template', templateSchema)