const mongoose = require('mongoose');
const { dbURI } = require('./config');

async function initConnection() {
  try {
    await mongoose.connect(dbURI, {
      dbName: 'crunch-task',
    });
    console.log('db connected successfuly')
  } catch (error) {
    console.log(error);
  }
}

module.exports = initConnection;